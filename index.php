<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>BlackheartDev</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- ====Favicons==== -->
    <link rel="shortcut icon" href="favicon.png" type="image/x-icon">
    <link rel="icon" href="favicon.png" type="image/x-icon">
    
    <!-- ==== Google Font ==== -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,500,700,800" rel="stylesheet">

    <!-- ==== Font Awesome ==== -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    
    <!-- ==== Bootstrap ==== -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    
    <!-- ==== jQuery UI ==== -->
    <link href="css/jquery-ui.min.css" rel="stylesheet">
    
    <!-- ==== Owl Carousel Plugin ==== -->
    <link href="css/owl.carousel.min.css" rel="stylesheet">
    
    <!-- ====Main Stylesheet==== -->
    <link href="style.css" rel="stylesheet">
    
    <!-- ====Custom Stylesheet==== -->
    <link href="css/custom-style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="js/html5shiv.min.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->

</head>
<body data-spy="scroll" data-target=".navbar" data-offset="100">
    <!-- Wrapper Start -->
    <div class="wrapper">
        <!-- Preloader Start -->
        <div id="preloader">
            <div class="preloader--bounce">
                <div class="preloader-bouncer--1"></div>
                <div class="preloader-bouncer--2"></div>
            </div>
        </div>
        <!-- Preloader End -->
        
        <!-- Header Area Start -->
        <div id="header">
            <nav class="header--navbar">
                <div class="container">
                    <div class="navbar">
                        <div class="navbar-header">
                            <!-- Header Custom Button Start -->
                            <div class="header--custom-btn hidden-lg hidden-md">
                                <a href="#hireMeModal" class="btn--default" data-toggle="modal"><i class="fa fa-user-plus"></i></a>
                            </div>
                            <!-- Header Custom Button End -->
                        
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#headerNav" aria-expanded="false" aria-controls="headerNav">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            
                            <!-- Logo Start -->
                            <a class="logo navbar-brand" href="#">
                                <div class="logo--img">
                                    <img src="img/logo.png" class="img-responsive" alt="">
                                </div>
                                
                                <div class="logo--content">
                                    <h1>NUKHKADI <strong>RABAZANOV</strong></h1>
                                    <p><strong>DEVELOPER</strong>/DESIGNER</p>
                                </div>
                            </a>
                            <!-- Logo End -->
                        </div>
                        
                        <!-- Header Custom Button Start -->
                        <div class="header--custom-btn hidden-sm hidden-xs">
                            <a href="#hireMeModal" class="btn--default" data-toggle="modal">НАНЯТЬ</a>
                        </div>
                        <!-- Header Custom Button End -->
                        
                        <!-- Header Nav Start -->
                        <div id="headerNav" class="header--nav navbar-collapse collapse">
                            <ul class="nav navbar-nav navbar-right AnimateScroll">
                                <li><a href="#banner">ГЛАВНАЯ</a></li>
                                <li><a href="#about">О СЕБЕ</a></li>
                                <li><a href="#services">НАВЫКИ</a></li>
                                <li><a href="#gallery">РАБОТЫ</a></li>
                                <li><a href="#feedback">ОТЗЫВЫ</a></li>
                                <li><a href="#contact">КОНТАКТЫ</a></li>
                            </ul>
                        </div>
                        <!-- Header Nav End -->
                    </div>
                </div>
            </nav>
        </div>
        <!-- Header Area End -->
        
        <!-- Banner Area Start -->
        <div id="banner" data-parallax-bg-img="img/banner-img/banner.jpg" data-position-y="0px">
            <div class="container">
                <div class="vc--parent">
                	<div class="vc--child">
                        <!-- Banner Content Start -->
                        <div class="banner--content">
                            <h2>Программист<span class="thick"> Это </span> Нечто большее, <span class="thick">Чем</span> Заработок</h2>
                            
                            <p>Я разработчик программного обеспечения из <a href="https://ru.wikipedia.org/wiki/%D0%9D%D0%B8%D0%B6%D0%BD%D0%B5%D0%B2%D0%B0%D1%80%D1%82%D0%BE%D0%B2%D1%81%D0%BA" target="_blank"><strong>#Нижневартовска</strong></a>. Работаю на различных платформах от WEB до Mobile.</p>
                            
                            <a href="#contact" class="btn--default AnimateScrollLink">Написать!</a>
                        </div>
                        <!-- Banner Content End -->
                    </div>
                </div>
            </div>
        </div>
        <!-- Banner Area End -->
            
        <!-- About Area Start -->
        <div id="about">
            <div class="container">
                <!-- Section Title Start -->
                <div class="section--title">
                    <h2><strong>НЕМНОГО ОБО МНЕ</strong></h2>
                </div>
                <!-- Section Title End -->
                
                <div class="row">
                    <div class="col-md-6">
                        <h3>КТО <strong>Я</strong>?</h3>

                        <p>Я Рабазанов Нухкади Магомедович. Программист-фрилансер. Родился в городе Радужный в Ханты-Мансийском автономном округе - Югра, позже переехал в Нижневартовск для обучения</p>
                        
                        <p>Компьютерами интересовался с 15 лет и только с 17 лет начал изучать основы программирования на c++. В 20 лет получил статус эксперта WorldSkills по компетенции "Программное решение для бизнеса"</p>
                        
                        <a href="https://sp.dp.worldskills.ru/q3wXY795PK7" class="btn btn--primary" download>Посмотреть SkillsPassport</a>
                    </div>
                    
                    <div class="col-md-6">
                        <h3>МОИ <strong>НАВЫКИ</strong></h3>

                        <p>Данный список всегда пополняется! Очень важно идти в ногу со временем и новыми технологиями</p>
                        
                        <!-- About Progrees Items Start -->
                        <div class="about--progress-items">
                            <!-- About Progrees Bar Start -->
                            <h4>DESKTOP-РАЗРАБОТКА</h4>
                            
                            <div class="progress">
                                <div class="progress-bar" data-progress="90"><span>90%</span></div>
                            </div>
                            <!-- About Progrees Bar End -->
                            
                            <!-- About Progrees Bar Start -->
                            <h4>WEB-РАЗРАБОТКА</h4>
                            
                            <div class="progress">
                                <div class="progress-bar" data-progress="70"><span>70%</span></div>
                            </div>
                            <!-- About Progrees Bar End -->
                        
                            <!-- About Progrees Bar Start -->
                            <h4>MOBILE-РАЗРАБОТКА</h4>
                                
                            <div class="progress">
                                <div class="progress-bar" data-progress="70"><span>70%</span></div>
                            </div>
                            <!-- About Progrees Bar End -->
                        
                            <!-- About Progrees Bar Start -->
                            <h4>БАЗЫ ДАННЫХ</h4>
                                
                            <div class="progress">
                                <div class="progress-bar" data-progress="100"><span>100%</span></div>
                            </div>
                            <!-- About Progrees Bar End -->
                        </div>
                        <!-- About Progrees Items End -->
                    </div>
                </div>
                
                <!-- About Info Start -->
                <div class="about--info">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="about--info-wrapper">
                                <h3><strong>ОБРАЗОВАНИЕ</strong></h3>
                                
                                <!-- About Info Item Start -->
                                <div class="about--info-item">
                                    <div class="row reset-gutter">
                                        <div class="col-xs-4">
                                            <p>2016-2020</p>
                                            <h5><strong>НИЖНЕВАРТОВСКИЙ СОЦИАЛЬНО-ГУМАНИТАРНЫЙ КОЛЛЕДЖ</strong></h5>
                                        </div>
                                        
                                        <div class="col-xs-8 about--info-border">
                                            <h4><strong>ПРОГРАММИСТ</strong>-ТЕХНИК</h4>
                                            
                                            <p>Окончил колледж на красный диплом выпустив проект TimeTabler и получив "отлично" с особым мнением комиссии</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- About Info Item End -->
                                
                                <!-- About Info Item Start -->
                                <div class="about--info-item">
                                    <div class="row reset-gutter">
                                        <div class="col-xs-4">
                                            <p>2021-2021</p>
                                            <h5><strong>АКАДЕМИЯ WORLDSKILLS ГОРОДА МОСКВА</strong></h5>
                                        </div>
                                        
                                        <div class="col-xs-8 about--info-border">
                                            <h4><strong>ПРОГРАММНЫЕ</strong> РЕШЕНИЯ ДЛЯ БИЗНЕСА</h4>
                                            
                                            <p>Успешно сдал демоэкзамен по компетенции WorldSkills по направлению "Программное решение для бизнеса" и получил статус эксперта</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- About Info Item End -->
                                
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="about--info-wrapper">
                                <h3><strong>ОПЫТ</strong></h3>
                                
                                <!-- About Info Item Start -->
                                <div class="about--info-item">
                                    <div class="row reset-gutter">
                                        <div class="col-xs-4">
                                            <p>2019-2021</p>
                                            <h5><strong>АКАДЕМИЯ ДИЗАЙНА И ПРОГРАММИРОВАНИЯ ГОРОДА НОВОСИБИРСК</strong></h5>
                                        </div>
                                        
                                        <div class="col-xs-8 about--info-border">
                                            <h4>ПРЕПОДАВАТЕЛЬ <strong>ПРОГРАММИРОВАНИЯ</strong></h4>
                                            
                                            <p>Отработал 2 учебных года в частной академии преподавателем программировани и вёрстки сайтов</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- About Info Item End -->
                                
                                <!-- About Info Item Start -->
                                <div class="about--info-item">
                                    <div class="row reset-gutter">
                                        <div class="col-xs-4">
                                            <p>2020-2021</p>
                                            <h5><strong>НИЖНЕВАРТОВСКИЙ СОЦИАЛЬНО-ГУМАНИТАРНЫЙ КОЛЛЕДЖ</strong></h5>
                                        </div>
                                        
                                        <div class="col-xs-8 about--info-border">
                                            <h4>ПРЕПОДАВАТЕЛЬ<strong></strong></h4>
                                            
                                            <p>На данный момент работаю преподавателем информатики, операционных систем, архитектуры аппаратных средств и программирования на Python</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- About Info Item End -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- About Info End -->
            </div>
        </div>
        <!-- About Area End -->
            
        <!-- Services Area Start -->
        <div id="services" class="bg--overlay" data-parallax-bg-img="img/services-img/bg.jpg">
            <div class="container">
                <!-- Section Title Start -->
                <div class="section--title">
                    <h2><strong>ЧТО Я МОГУ?</strong></h2>
                </div>
                <!-- Section Title End -->
                
                <div class="row row-eq-height">
                    <!-- Service Item Start -->
                    <div class="col-md-4 col-sm-6 service--item">
                        <div class="service--icon">
                            <i class="fa fa-laptop"></i>
                        </div>
                        
                        <div class="service--content">
                            <h2><strong>WEB </strong>РАЗРАБОТКА</h2>
                            
                            <p>Разработка сайтов (JS-jquery, ajax и PHP) и выгрузка на хостинг с последующим обслуживанием</p>
                        </div>
                    </div>
                    <!-- Service Item End -->
                    
                    <!-- Service Item Start -->
                    <div class="col-md-4 col-sm-6 service--item">
                        <div class="service--icon">
                            <i class="fa fa-object-group"></i>
                        </div>
                        
                        <div class="service--content">
                            <h2><strong>DESKTOP </strong>РАЗРАБОТКА</h2>
                            
                            <p>Разработка приложений для ПК на языке C# с использованием платформы ADO.NET</p>
                        </div>
                    </div>
                    <!-- Service Item End -->
                
                    <!-- Service Item Start -->
                    <div class="col-md-4 col-sm-6 service--item">
                        <div class="service--icon">
                            <i class="fa fa-mobile"></i>
                        </div>
                        
                        <div class="service--content">
                            <h2><strong>MOBILE</strong> РАЗРАБОТКА</h2>
                            
                            <p>Разработка кроссплатформенных мобильных приложений на фреймворке Flutter с использованием GoLang.</p>
                        </div>
                    </div>
                    <!-- Service Item End -->
                </div>
            </div>
        </div>
        <!-- Services Area End -->
            
        <!-- Gallery Area Start -->
        <div id="gallery">
            <div class="container">
                <!-- Section Title Start -->
                <div class="section--title">
                    <h2><strong>ГОТОВЫЕ ПРОЕКТЫ</strong></h2>
                </div>
                <!-- Section Title End -->
                
                <!-- Gallery Filter Menu Start -->
                <div class="gallery-filter-menu">
                    <ul>
                        <li class="active"><a href="*" class="btn--default">ВСЕ ПРОЕКТЫ</a></li>
                        <li><a href="website-item" class="btn--default">ВЕБ-САЙТЫ</a></li>
                        <li><a href="logo-item" class="btn--default">MOBILE-ПРОЕКТЫ</a></li>
                        <li><a href="application-item" class="btn--default">DESKTOP-ПРОЕКТЫ</a></li>
                        <li><a href="illustation-item" class="btn--default">СЕРВЕРА</a></li>
                        <li><a href="ai-item" class="btn--default">AI-ПРОЕКТЫ</a></li>
                    </ul>
                </div>
                <!-- Gallery Filter Menu End -->
                
                <!-- Gallery Items Start -->
                <div class="row gallery-items">
                    <!-- Gallery Item Start -->
                    <div class="col-xs-12 col-sm-4 col-md-3 thumbnail gallery-item" data-cat="logo-item">
                        <img src="img/gallery-img/gallery-photo-1.png" alt="" />
                        
                        <div class="gallery-overlay">
                            <div class="vc--parent">
                            	<div class="vc--child">
                                    <h2>TimeTabler</h2>
                                    
                                    <p>Приложение для просмотра расписания занятий в учебном учереждении</p>
                                    
                                    <a href="#galleryDetails1" class="btn gallery--window" data-toggle="modal">Посмотреть</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Gallery Item End -->
                    
                    
                    <!-- Gallery Item Start -->
                    <div class="col-xs-12 col-sm-4 col-md-3 thumbnail gallery-item" data-cat="website-item">
                        <img src="img/gallery-img/gallery-photo-3.png" alt="" />
                        
                        <div class="gallery-overlay">
                            <div class="vc--parent">
                            	<div class="vc--child">
                                    <h2>Сайт GreenFix</h2>
                                    
                                    <p>Сайт для сервиса по ремонту компьютерной техники GreenFix</p>
                                    
                                    <a href="#galleryDetails3" class="btn gallery--window" data-toggle="modal">Посмотреть</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Gallery Item End -->
                    
                    <!-- Gallery Item Start -->
                    <div class="col-xs-12 col-sm-4 col-md-3 thumbnail gallery-item" data-cat="application-item">
                        <img src="img/gallery-img/gallery-photo-4.png" alt="" />
                        
                        <div class="gallery-overlay">
                            <div class="vc--parent">
                            	<div class="vc--child">
                                    <h2>FITiM</h2>
                                    
                                    <p>Приложение для диспетчера расписаний учебного заведения для автоматизации его работы</p>
                                    
                                    <a href="#galleryDetails4" class="btn gallery--window" data-toggle="modal">Посмотреть</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Gallery Item End -->

                    <!-- Gallery Item Start -->
                    <div class="col-xs-12 col-sm-4 col-md-3 thumbnail gallery-item" data-cat="ai-item">
                        <img src="img/gallery-img/gallery-photo-5.png" alt="" />
                        
                        <div class="gallery-overlay">
                            <div class="vc--parent">
                            	<div class="vc--child">
                                    <h2>PyTextAnalyze</h2>
                                    
                                    <p>Сервис для оценки эмоционального окраса по тексту с помощью нейронной сети</p>
                                    
                                    <a href="#galleryDetails5" class="btn gallery--window" data-toggle="modal">Посмотреть</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Gallery Item End -->
                </div>
                <!-- Gallery Items End -->
            </div>
        
            <!-- Gallery Details Modal Start -->
            <div class="modal fade gallery--details" id="galleryDetails1" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal Content Start -->
                    <div class="modal-content">
                        <!-- Modal Header Start -->
                        <div class="modal-header">
                            <span class="close" data-dismiss="modal">&times;</span>
                            
                            <h4 class="modal-title">TimeTabler</h4>
                            <p>Приложение для автоматизации работы с расписанием</p>
                        </div>
                        <!-- Modal Header End -->
                        
                        <!-- Modal Body Start -->
                        <div class="modal-body">
                            <p>Проект разрабатывается на фреймворке flutter<br>Вместо скринов, предлагаю вам посмотреть видеоролик рассказывающий об этом проекте</p>
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/Tnid4kegaxg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                        <!-- Modal Body End -->
                        
                        <!-- Modal Footer Start -->
                        <div class="modal-footer gallery--description">
                            <p>Проект ещё в разработке!<br>Тип проекта: Закрытый<br>Примерный срок выпуска: Январь 2022 года</p>
                        </div>
                        <!-- Modal Footer End -->
                    </div>
                    <!-- Modal Content End -->
                </div>
            </div>
            <!-- Gallery Details Modal End -->

            <!-- Gallery Details Modal Start -->
            <div class="modal fade gallery--details" id="galleryDetails3" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal Content Start -->
                    <div class="modal-content">
                        <!-- Modal Header Start -->
                        <div class="modal-header">
                            <span class="close" data-dismiss="modal">&times;</span>
                            
                            <h4 class="modal-title">GreenFix</h4>
                            <p>Сайт-визитка для сервиса по ремонту компьютеров GreenFix</p>
                        </div>
                        <!-- Modal Header End -->
                        
                        <!-- Modal Body Start -->
                        <div class="modal-body">
                            <p>В разработке использовались следующие языки:<br>
                            - HTML+CSS(Bootstrap, AwesomeIcons)<br>
                            - JS(jQuery, AjaxJS)<br>
                            - SQL(PostgreSQL)<br>
                        При отправлении заявки появляется модальное окно для взаимодействия с БД, которая никак не влияет на восприятие самого интерфейса сайта, тем самым не перегружая его.</p>
                        <a class="colorbox" data-fancybox="group-1" href="/img/gf1.png" >
                            <img src="/img/gf1.png" />
                       </a>
                       <p><br></p>
                       <a class="colorbox" data-fancybox="group-1" href="/img/gf2.png" >
                            <img src="/img/gf2.png" />
                       </a>
                       <p><br></p>
                       <a class="colorbox" data-fancybox="group-1" href="/img/gf3.png" >
                            <img src="/img/gf3.png" />
                       </a>
                        </div>
                        <!-- Modal Body End -->
                        
                        <!-- Modal Footer Start -->
                        <div class="modal-footer gallery--description">
                            <p>Проект завершён!<br>Тип проекта: Закрытый<br>Дата завершения: май 2021</p>
                        </div>
                        <!-- Modal Footer End -->
                    </div>
                    <!-- Modal Content End -->
                </div>
            </div>
            <!-- Gallery Details Modal End -->

            <!-- Gallery Details Modal Start -->
            <div class="modal fade gallery--details" id="galleryDetails4" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal Content Start -->
                    <div class="modal-content">
                        <!-- Modal Header Start -->
                        <div class="modal-header">
                            <span class="close" data-dismiss="modal">&times;</span>
                            
                            <h4 class="modal-title">FITiM</h4>
                            <p>Приложение для автоматизации работы диспетчера расписаний</p>
                        </div>
                        <!-- Modal Header End -->
                        
                        <!-- Modal Body Start -->
                        <div class="modal-body">
                            <p>Работа включает в себя полный функционал для добавления расписания и отслеживания уже существующего расписания<br>
                                <a class="colorbox" data-fancybox="group-1" href="/img/fitim1.png" >
                                    <img src="/img/fitim1.png" />
                               </a>
                               <br>
                               <a class="colorbox" data-fancybox="group-1" href="/img/fitim2.png" >
                                    <img src="/img/fitim2.png" />
                               </a>
                               <br>
                               <a class="colorbox" data-fancybox="group-1" href="/img/fitim3.png" >
                                    <img src="/img/fitim3.png" />
                               </a>
                            </p>
                        </div>
                        <!-- Modal Body End -->
                        
                        <!-- Modal Footer Start -->
                        <div class="modal-footer gallery--description">
                            <p>Проект завершён!<br>Тип проекта: Закрытый<br>Дата завершения: июнь 2021</p>
                        </div>
                        <!-- Modal Footer End -->
                    </div>
                    <!-- Modal Content End -->
                </div>
            </div>
            <!-- Gallery Details Modal End -->

            <!-- Gallery Details Modal Start -->
            <div class="modal fade gallery--details" id="galleryDetails5" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal Content Start -->
                    <div class="modal-content">
                        <!-- Modal Header Start -->
                        <div class="modal-header">
                            <span class="close" data-dismiss="modal">&times;</span>
                            
                            <h4 class="modal-title">EmoticonBot</h4>
                            <p>Нейронная сеть, написанная на python ,предназначенная для анализа эмоционального окраса в тексте</p>
                        </div>
                        <!-- Modal Header End -->Ы
                        
                        <!-- Modal Body Start -->
                        <div class="modal-body">
                            <p>Исскуственный интеллект получает данные в виде строки и анализирует текст отзыва на эмоциальный окрас. Далее выводит соотношение окраса в процентах и даёт рекомендации менеджеру<br>Ссылка на проект в GitLab<br><a href="https://gitlab.com/Blackheart_Inside/ai-textanalyzator-py">https://gitlab.com/Blackheart_Inside/ai-textanalyzator-py</a></p>
                            <p><br></p>
                            <a class="colorbox" data-fancybox="group-1" href="/img/pyAi1.jpg" >
                                <img src="/img/pyAi1.jpg" />
                           </a>
                           <p><br></p>
                           <a class="colorbox" data-fancybox="group-1" href="/img/pyAi2.jpg" >
                            <img src="/img/pyAi2.jpg" />
                            </a>
                        </div>
                        <!-- Modal Body End -->
                        
                        <!-- Modal Footer Start -->
                        <div class="modal-footer gallery--description">
                            <p>Проект завершён!<br>Тип проекта: открытый</p>
                        </div>
                        <!-- Modal Footer End -->
                    </div>
                    <!-- Modal Content End -->
                </div>
            </div>
            <!-- Gallery Details Modal End -->
        </div>
        <!-- Gallery Area End -->
            
        <!-- Feedback Area Start -->
        <div id="feedback" data-parallax-bg-img="img/feedback-img/bg.png">
            <div class="container">
                <div class="row">
                    <!-- Feedback FAQ Start -->
                    <div class="col-md-4 feedback--faq">
                        <!-- Feedback Title Start -->
                        <h3 class="feedback--title">ПОЧЕМУ Я?</h3>
                        <!-- Feedback Title End -->
                        
                        <!-- Feedback Accordion Start -->
                        <div class="panel-group" id="feedbackA" role="tablist">
                            <!-- Feedback Accordion Item Start -->
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <a href="#feedbackA01" data-toggle="collapse" data-parent="#feedbackA">
                                        <h4 class="panel-title">Стремление к изучению нового <i class="fa fa-minus"></i></h4>
                                    </a>
                                </div>
                                
                                <div id="feedbackA01" class="panel-collapse collapse in" role="tabpanel">
                                    <div class="panel-body">
                                        Все мы знаем, что в IT-сфере очень часто появляются новые технологии и методы реализации тех или иных задач, поэтому программистам очень важно быть всегда в кур
                                    </div>
                                </div>
                            </div>
                            <!-- Feedback Accordion Item End -->
                            
                            <!-- Feedback Accordion Item Start -->
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <a href="#feedbackA02" class="collapsed" data-toggle="collapse" data-parent="#feedbackA">
                                        <h4 class="panel-title">Идеализирование<i class="fa fa-minus"></i></h4>
                                    </a>
                                </div>
                                
                                <div id="feedbackA02" class="panel-collapse collapse" role="tabpanel">
                                    <div class="panel-body">
                                        Для меня важно, чтоб все мои работы были сделаны идеально, может дойти до того, что если меня не полностью устраивает моя выполненная работа, то я прошу ещё несколько дней, чтоб довести работу до идеала!
                                    </div>
                                </div>
                            </div>
                            <!-- Feedback Accordion Item End -->
                            
                            <!-- Feedback Accordion Item Start -->
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <a href="#feedbackA03" class="collapsed" data-toggle="collapse" data-parent="#feedbackA">
                                        <h4 class="panel-title">Современные технологии <i class="fa fa-minus"></i></h4>
                                    </a>
                                </div>
                                
                                <div id="feedbackA03" class="panel-collapse collapse" role="tabpanel">
                                    <div class="panel-body">
                                        В своих работах я использую только современные технологии для долгой актуальности моей работы
                                    </div>
                                </div>
                            </div>
                            <!-- Feedback Accordion Item End -->
                        </div>
                        <!-- Feedback Accordion End -->
                    </div>
                    <!-- Feedback FAQ End -->
                    
                    <!-- Feedback Items Start -->
                    <div class="col-md-8 feedback--items">
                        <!-- Feedback Title Start -->
                        <h3 class="feedback--title">ОТЗЫВЫ</h3>
                        <!-- Feedback Title End -->
                        
                        <div class="feedback--slider FeedbackSlider">
                            <!-- Feedback Item Start -->
                            <div class="feedback--item" data-client-img="img/feedback-img/client-01.jpg">
                                <i class="fa fa-quote-left"></i>
                                
                                <p>Отзывов пока нет</p>
                                
                                <span class="cite">Будьте первым!</span>
                            </div>
                            <!-- Feedback Item End -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Feedback Area End -->
    
        <!-- Contact Area Start -->
        <div id="contact">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <!-- Contact Address Start -->
                        <div class="contact--address">
                            <h2>КОНТАКТЫ</h2>
                            
                            <address>
                                <p><i class="fa fa-home"></i>Нижневартовск</p>
                                <p><i class="fa fa-envelope"></i>greydan0586@gmail.com</p>
                                <p><i class="fa fa-phone"></i>+7(922)658-36-34</p>
                            </address>
                        </div>
                        <!-- Contact Address End -->
                            
                        <!-- Contact Social Start -->
                        <div class="contact--social">
                            <ul>
                                <li><a href="https://www.instagram.com/blackheart_inside"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="https://vk.com/blackheart_inside"><i class="fa fa-vk"></i></a></li>
                            </ul>
                        </div>
                        <!-- Contact Social End -->
                    </div>
                    
                    <div class="col-sm-6">
                        <!-- Contact Form Start -->
                        <div class="contact--form">
                            <div class="contact-form-status"></div>
                            
                            <form action="contact.php" method="post">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" name="contactName" class="form-control" placeholder="Имя">
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="email" name="contactEmail" class="form-control" placeholder="Почта">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <input type="text" name="contactSubject" class="form-control" placeholder="Организация">
                                </div>
                                
                                <div class="form-group">
                                    <textarea name="contactMessage" class="form-control" rows="9" placeholder="Сообщение"></textarea>
                                </div>
                                
                                <input type="submit" value="ОТПРАВИТЬ" class="submit-btn btn--primary">
                            </form>
                        </div>
                        <!-- Contact Form End -->
                    </div>
                </div>
            </div>
        </div>
        <!-- Contact Area End -->
        
        <!-- Footer Area Start -->
        <div id="footer">
            <div class="container">
                <!-- Footer Copyright Start -->
                <div class="footer--copyright">
                    <p>Dev by Blackheart.</p>
                </div>
                <!-- Footer Copyright End -->
            </div>
        </div>
        <!-- Footer Area End -->
        
        <!-- Back To Top Area Start -->
        <div id="backToTop">
            <a href="#banner" class="btn--default AnimateScrollLink"><i class="fa fa-angle-up"></i></a>
        </div>
        <!-- Back To Top Area End -->
        
        <!-- Hire Me Modal Start -->
        <div class="hire-me--modal modal fade" id="hireMeModal" role="dialog">
            <div class="modal-dialog modal-lg">
                <!-- Modal Content Start -->
                <div class="modal-content">
                    <!-- Modal Header Start-->
                    <div class="modal-header">
                        <span class="close" data-dismiss="modal">&times;</span>
                        
                        <h4 class="modal-title">НУЖЕН ПРОЕКТ?</h4>
                        <p>Я готов вам помочь! Уточните детали ниже или отправьте файл</p>
                    </div>
                    <!-- Modal Header End-->
                    
                    <!-- Modal Body Start-->
                    <div class="modal-body">
                        <form action="hire-me.php" method="post" id="popupContactForm">
                            <div class="row">
                                <div class="col-md-8 form-controls">
                                    <div class="row">
                                        <div class="col-md-6 name">
                                            <input type="text" name="name" placeholder="Ваше имя..." />
                                        </div>
                                        
                                        <div class="col-md-6 email">
                                            <input type="email" name="email" placeholder="Ваша почта..." />
                                        </div>
                                        
                                        <div class="col-md-6 project-title">
                                            <input type="text" name="project_name" placeholder="Название проекта..." />
                                        </div>
                                        
                                        <div class="col-md-6 case-category">
                                            <select name="category" class="bg-fa-caret-down SelectMenu">
                                                <option value="">-Категория-</option>
                                                <option value="mobile">Мобильное приложение</option>
                                                <option value="desktop">Десктоп-приложение</option>
                                                <option value="website">Веб-сайт</option>
                                                <option value="server">Сервер</option>
                                            </select>
                                        </div>
                                        
                                        <div class="col-md-6 budget">
                                            <input type="number" name="budget" placeholder="Бюджет" />
                                        </div>
                                        
                                        <div class="col-md-6 date">
                                            <input type="text" name="date" class="DatePicker bg-fa-calender" placeholder="-Желаемая дата сдачи-" />
                                        </div>
                                        
                                        <div class="col-md-12 message">
                                            <textarea name="message" rows="9">Ваше сообщение...</textarea>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-4 estimate-container">
                                    <div class="estimate-img">
                                        <img src="img/hire-me-pic.png" alt="" class="center-block img-responsive"/>
                                    </div>
                                    
                                    <div class="estimate-text">
                                        <h4>Требуется ли оценка?</h4>
                                        
                                        <div class="radio">
                                          <input type="radio" name="estimate" value="no" checked="checked" id="estimateZERO"><label for="estimateZERO">Оценка не нужна</label>
                                        </div>
                                        
                                        <div class="radio">
                                          <input type="radio" name="estimate" value="yes" id="estimateONE"><label for="estimateONE">
                                            Да, мне нужна сметная стоимость услуг. Я понимаю, что для получения сметы мне нужно 2–4 рабочих дня после ответа на заявку.</label>
                                        </div>
                                        
                                        <input type="submit" value="ОТПРАВИТЬ ЗАЯВКУ" class="submit-btn btn--primary btn-block" />
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- Modal Body End-->
                </div>
                <!-- Modal Content End -->
            </div>
        </div>
        <!-- Hire Me Modal End -->
    </div>
    <!-- Wrapper End -->

    <script src="js/zoomImg.js"></script>
    <!-- ==== jQuery ==== -->
    <script src="js/jquery.min.js"></script>

    <!-- ==== Bootstrap ==== -->
    <script src="js/bootstrap.min.js"></script>

    <!-- ==== jQuery UI DatePicker Plugin ==== -->
    <script src="js/jquery-ui.min.js"></script>

    <!-- ==== Owl Carousel Plugin ==== -->
    <script src="js/owl.carousel.min.js"></script>

    <!-- ==== Isotope Plugin ==== -->
    <script src="js/isotope-docs.min.js"></script>
    
    <!-- ==== jQuery Form Plugin ==== -->
    <script src="js/jquery.form.min.js"></script>
    
    <!-- ==== jQuery Validation Plugin ==== -->
    <script src="js/jquery.validate.min.js"></script>

    <!-- ==== Google Map API ==== -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBK9f7sXWmqQ1E-ufRXV3VpXOn_ifKsDuc"></script>
    
    <!-- ==== GMaps Plugin ==== -->
    <script src="js/gmaps.min.js"></script>
    
    <!-- ==== jQuery Waypoints Plugin ==== -->
    <script src="js/jquery.waypoints.min.js"></script>
    
    <!-- ==== Animate Scroll Plugin ==== -->
    <script src="js/animatescroll.min.js"></script>
    
    <!-- ==== CounterUp Plugin ==== -->
    <script src="js/jquery.counterup.min.js"></script>
    
    <!-- ==== jQuery Nice Scroll Plugin ==== -->
    <script src="js/jquery.nicescroll.min.js"></script>
    
    <!-- ==== Parallax Plugin ==== -->
    <script src="js/parallax.min.js"></script>
    
    <!-- ==== RetinaJS Plugin ==== -->
    <script src="js/retina.min.js"></script>

    <!-- ==== Main Script ==== -->
    <script src="js/main.js"></script>
    <script src="colorbox/jquery.colorbox-min.js"></script>
        <link rel="stylesheet" type="text/css" href="colorbox/example5/colorbox.css">

<script>
$(function(){
    $("a.colorbox").colorbox({maxWidth:"90%",maxHeight:"90%",current:"Фото {current} из {total}"});
});
</script>
</body>
</html>
